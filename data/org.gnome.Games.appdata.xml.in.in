<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright 2015 Adrien Plazas -->
<component type="desktop-application">
  <id>@appid@</id>
  <metadata_license>CC0-1.0</metadata_license>
  <project_license>GPL-3.0+</project_license>
  <name>Games</name>
  <summary>Simple game launcher for GNOME</summary>

  <description>
    <p>
      Games is a GNOME 3 application to browse your video games library and to
      easily pick and play a game from it. It aims to do for games what Music
      already does for your music library.
    </p>
    <p>
      You want to install Games if you just want a very simple and comfortable
      way to play your games and you don’t need advanced features such as
      speedrunning tools or video game development tools.
    </p>
    <p>Features:</p>
    <ul>
      <li>List your installed games, your Steam games, your game ROMs…</li>
      <li>Search in your games collection</li>
      <li>Play your games</li>
      <li>Resume your game to where you left it</li>
    </ul>
  </description>

  <screenshots>
    <screenshot type="default">
      <image width="1313" height="802">https://gitlab.gnome.org/GNOME/gnome-games/raw/master/data/appdata/3-36-games-collection.png</image>
    </screenshot>
    <screenshot>
      <image width="1313" height="802">https://gitlab.gnome.org/GNOME/gnome-games/raw/master/data/appdata/3-36-platforms.png</image>
    </screenshot>
    <screenshot>
      <image width="1313" height="802">https://gitlab.gnome.org/GNOME/gnome-games/raw/master/data/appdata/3-36-retro-snapshots.png</image>
    </screenshot>
    <screenshot>
      <image width="852" height="599">https://gitlab.gnome.org/GNOME/gnome-games/raw/master/data/appdata/3-36-gamepad-mapping.png</image>
    </screenshot>
  </screenshots>

  <releases>
    <release version="40.0" type="stable" date="2021-03-20" urgency="medium">
      <description>
        <p>Improvements:</p>
        <ul>
          <li>Support running Nintendo 3DS games</li>
          <li>Support running Sega Dreamcast games (gdi and cdi)</li>
          <li>Use HdyStatusPage for empty states and errors</li>
          <li>Use nearest scaling for low-resolution icons</li>
          <li>Polish the screen layout switcher</li>
        </ul>
        <p>Fixes:</p>
        <ul>
          <li>Support saves when the core doesn't support savestates</li>
          <li>Don't save a corrupted snapshot if failed mid-save</li>
          <li>Correctly remove screen layout switcher on error pages</li>
          <li>Fix a critical when trying to run a game that's missing a core</li>
        </ul>
        <p>Translation updates.</p>
      </description>
    </release>
    <release version="3.38.0" type="stable" date="2020-09-12" urgency="medium">
      <description>
        <p>Improvements:</p>
        <ul>
          <li>Support running Nintendo 64 games
            <ul>
              <li>Show a menu for switching controller expansion paks</li>
            </ul>
          </li>
          <li>Introduce collections
            <ul>
              <li>Favorites</li>
              <li>Recently Played</li>
              <li>Manual collections</li>
              <li>Favorites</li>
              <li>Selection mode to manage collections and favorite games</li>
            </ul>
          </li>
          <li>Run games in a secondary process</li>
          <li>Show an error if a game crashes</li>
          <li>Cache collection in the database, dramatically improving loading times after the first run</li>
          <li>Speed up and reenable MAME plugin</li>
          <li>Cache game icons</li>
          <li>Add a search provider for GNOME Shell</li>
          <li>Add screen gap for Nintendo DS games when using top/bottom layout
            <ul>
              <li>Optimize the gap width for 15 known games, use 80px for others</li>
            </ul>
          </li>
          <li>Disable keyboard gamepad emulation for MS-DOS games, as most games use keyboard
            <ul>
              <li>Physical gamepads are still recognized as gamepads</li>
            </ul>
          </li>
          <li>Ask for confirmation when trying to restart a game that doesn't support snapshots</li>
          <li>Show a message when there are no search results</li>
          <li>Prevent headerbar from hiding while there are open popovers</li>
          <li>Show a warning when Tracker is not present</li>
          <li>Show analog sticks in controller preferences as moving dots, animate them in mapping mode</li>
          <li>Support back swipe:
            <ul>
              <li>In platforms view in mobile mode</li>
              <li>In preferences window in mobile mode</li>
              <li>In controller preferences</li>
            </ul>
          </li>
          <li>Switch to HdyWindow, allowing round bottom corners</li>
          <li>Support DBus activation</li>
          <li>Add a Ctrl+? shortcut for opening the shortcuts window</li>
          <li>Blur background behind game covers</li>
          <li>Overhaul preferences window to match HdyPreferencesWindow layout</li>
          <li>Remove delay before pausing the game after focusing out</li>
          <li>Make headerbar semi-transparent in fullscreen mode</li>
          <li>Update screenshots in appdata</li>
        </ul>
        <p>Fixes:</p>
        <ul>
          <li>Ensure analog works for pcsx_rearmed core</li>
          <li>Ensure loading notification is shown after unpausing</li>
          <li>Correctly create version file for 3.34.0 savestate migration</li>
          <li>Stop recalculating game titles when they're already cached</li>
          <li>Don't show headerbar menus for unsupported games</li>
          <li>Grab game view focus immediately after running a game</li>
          <li>Stop counting files like GameSomething.jpg as covers for Game.rom</li>
          <li>Don't open files when a game is already running</li>
          <li>Fix --help command line option output</li>
          <li>Use display name instead of basename for user-facing strings</li>
          <li>Stop translating internal errors that are never shown in the UI</li>
          <li>Stop listing application/x-desktop MIME type in desktop file</li>
          <li>Stop using core descriptor as URI for standalone libretro games</li>
          <li>Fix desktop game detection</li>
          <li>Fix criticals after removing games</li>
          <li>Properly reveal the titlebar after exiting fullscreen mode</li>
          <li>Fix Ctrl+Q shortcut on non-latin keyboard layouts</li>
        </ul>
        <p>Translation updates.</p>
      </description>
    </release>
    <release version="3.36.0" type="stable" date="2020-03-07" urgency="medium">
      <description>
        <p>Improvements:</p>
        <ul>
          <li>Collection should now load faster and with less UI stuttering:
            <ul>
              <li>Collection and cover loading are now threaded instead of using Vala's async functions</li>
              <li>Game runners are now created on demand when starting a game</li>
              <li>Steam plugin was simplified and should work a bit faster</li>
            </ul>
          </li>
          <li>New-style vertical covers are now supported for Steam games</li>
          <li>List savestate shortcuts in shortcuts dialog</li>
          <li>Use shorter date formats for savestates when possible</li>
          <li>Fix a grammar issue</li>
          <li>Stop styling Restore button in fullscreen mode</li>
          <li>Allow to restart the current game from secondary menu</li>
          <li>Rename savestates to snapshots</li>
          <li>Rename backup to export, restore to import</li>
          <li>Add a nightly icon</li>
          <li>Present sidebar as a content list in mobile mode</li>
          <li>Tweak lists: make them rounded, add separators and use thicker rows</li>
          <li>Use darker colors for the main window, matching other content apps</li>
        </ul>
        <p>Fixes:</p>
        <ul>
          <li>Don't show Steam tools, such as Proton, as games</li>
          <li>Skip junk files when migrating savestates from 3.32 or earlier</li>
          <li>MAME plugin now builds</li>
          <li>Fixed pointer input not working until saving the game</li>
          <li>Save directory is now properly loaded after a reset</li>
          <li>Nintendo DS screen mode is now reset to vertical after a reset</li>
          <li>Fixed opening savestate manager with GTK animations disabled</li>
          <li>Ensure bottom bar is always shown or hidden correctly on mobile</li>
          <li>Appdata now validates in strict mode</li>
          <li>Make controller use JOYPAD type regardless of having analog sticks</li>
          <li>Fix a runtime warning when starting standalone games</li>
          <li>Restore state and memory before running the first frame</li>
          <li>Fix a runtime error when loading a snapshot migrated from 3.32.x</li>
          <li>Don't allow type-to-search and Ctrl+F shortcuts with empty collection</li>
          <li>Fix libhandy and rsvg deprecations</li>
        </ul>
        <p>Translation updates.</p>
      </description>
    </release>
    <release version="3.34.0" type="stable" date="2019-09-09" urgency="medium">
      <description>
        <p>Improvements:</p>
        <ul>
          <li>Remove Developers view</li>
          <li>Add backup and restore functions</li>
          <li>Add mnemonics to primary menu</li>
          <li>Use adaptive view switcher in collection</li>
          <li>Allow to change screen layouts for Nintendo DS games when using DeSmuME and DeSmuME 2015 cores</li>
          <li>Replace plugin list in preferences by platform list, and allow to select libretro cores to be used for each platform</li>
          <li>Show an error when trying to open a non-game file</li>
          <li>Use a better icon for missing game thumbnails</li>
          <li>Main window is now fully adaptive</li>
          <li>Removed unused subtitles from collection</li>
          <li>Increased padding on platforms sidebar items</li>
          <li>Support multiple savestates for libretro games</li>
          <li>Only show fullscreen headerbar when cursor is nearby, sync behavior with libdazzle</li>
          <li>Hide cursor after a timeout in non-fullscreen mode</li>
          <li>Sync fullscreen restore button style with Adwaita, making it smaller and round</li>
          <li>Preferences window now has a Back button when the window is small</li>
        </ul>
        <p>Fixes:</p>
        <ul>
          <li>Help doesn't show up as a game anymore</li>
          <li>Pressing Enter key twice immediately after starting a game from a file doesn't exit the game anymore</li>
          <li>Explicitly specify 'm' dependency</li>
          <li>Keyboard shortcuts don't depend on keyboard layout anymore</li>
          <li>Game covers have proper colors now instead of being 10% darker</li>
          <li>Media button doesn't steal focus from the game anymore</li>
          <li>Miscellaneous UI fixes</li>
        </ul>
        <p>Translation updates.</p>
      </description>
    </release>
    <release version="3.32.0" type="stable" date="2019-03-10" urgency="medium">
      <description>
        <p>Improvements:</p>
        <ul>
          <li>Collection loading is now paused in game, leading to vastly improved performance when opening a game from file manager</li>
          <li>Disable MAME plugin because of major performance and reliability problems</li>
          <li>Revert to non-portal file chooser for Flatpak, to allow adding games consisting of multiple files</li>
          <li>Update primary menu layout according to app menu retirement initiative</li>
          <li>Allow to switch between gamepad and keyboard input in standalone Libretro games</li>
          <li>New application icons from GNOME Design team, symbolic icon is now used for HighContrast</li>
          <li>Many refinements in preferences window</li>
        </ul>
        <p>Fixes:</p>
        <ul>
          <li>It's not possible to open multiple main windows anymore</li>
          <li>Binding directional pads should work for more gamepads now</li>
          <li>Keyboard doesn't control every player in multiplayer games anymore</li>
          <li>Invalid discs in PlayStation disc selector are now disabled</li>
          <li>Quit dialog now actually appears when exiting a game that doesn't support snapshots</li>
          <li>Opening a game by double-clicking while another game is running now quits the previous game correctly</li>
          <li>Database file is now correctly created even if data directory didn't exist</li>
          <li>During search, developers and platforms without any games to show are now hidden</li>
          <li>Steam plugin reliability improvements</li>
        </ul>
        <p>Translation updates.</p>
      </description>
    </release>
  </releases>

  <provides>
    <binary>gnome-games</binary>
  </provides>

  <launchable type="desktop-id">@appid@.desktop</launchable>
  <url type="homepage">https://wiki.gnome.org/Apps/Games</url>
  <url type="bugtracker">https://gitlab.gnome.org/GNOME/gnome-games/issues</url>
  <url type="donation">http://www.gnome.org/friends/</url>
  <url type="translate">https://wiki.gnome.org/TranslationProject</url>
  <project_group>GNOME</project_group>
  <developer_name>The GNOME Project</developer_name>
  <update_contact>kekun.plazas_at_laposte.net</update_contact>
  <translation type="gettext">org.gnome.Games</translation>

  <content_rating type="oars-1.1">
    <content_attribute id="violence-cartoon">none</content_attribute>
    <content_attribute id="violence-fantasy">none</content_attribute>
    <content_attribute id="violence-realistic">none</content_attribute>
    <content_attribute id="violence-bloodshed">none</content_attribute>
    <content_attribute id="violence-sexual">none</content_attribute>
    <content_attribute id="violence-desecration">none</content_attribute>
    <content_attribute id="violence-slavery">none</content_attribute>
    <content_attribute id="violence-worship">none</content_attribute>
    <content_attribute id="drugs-alcohol">none</content_attribute>
    <content_attribute id="drugs-narcotics">none</content_attribute>
    <content_attribute id="drugs-tobacco">none</content_attribute>
    <content_attribute id="sex-nudity">none</content_attribute>
    <content_attribute id="sex-themes">none</content_attribute>
    <content_attribute id="sex-homosexuality">none</content_attribute>
    <content_attribute id="sex-prostitution">none</content_attribute>
    <content_attribute id="sex-adultery">none</content_attribute>
    <content_attribute id="sex-appearance">none</content_attribute>
    <content_attribute id="language-profanity">none</content_attribute>
    <content_attribute id="language-humor">none</content_attribute>
    <content_attribute id="language-discrimination">none</content_attribute>
    <content_attribute id="social-chat">none</content_attribute>
    <content_attribute id="social-info">none</content_attribute>
    <content_attribute id="social-audio">none</content_attribute>
    <content_attribute id="social-location">none</content_attribute>
    <content_attribute id="social-contacts">none</content_attribute>
    <content_attribute id="money-purchasing">none</content_attribute>
    <content_attribute id="money-gambling">none</content_attribute>
  </content_rating>
</component>
